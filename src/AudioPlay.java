class AudioPlayer implements Playable {
    @Override
    public void play() {
        System.out.println("Audio playback started.");
        // Code to start audio playback goes here
    }

    @Override
    public void stop() {
        System.out.println("Audio playback stopped.");
        // Code to stop audio playback goes here
    }
}
