interface Playable {
    void play();
    void stop();
}