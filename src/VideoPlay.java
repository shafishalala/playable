class VideoPlayer implements Playable {
    @Override
    public void play() {
        System.out.println("Video playback started.");
        // Code to start video playback goes here
    }

    @Override
    public void stop() {
        System.out.println("Video playback stopped.");
        // Code to stop video playback goes here
    }
}
